<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('0_templates.apps');
});

Route::get('/master/company', 'App\Http\Controllers\NavigationController@company_index');
Route::get('/master/unit', 'App\Http\Controllers\NavigationController@unit_index');
Route::get('/master/product', 'App\Http\Controllers\NavigationController@product_index');

Route::get('/transaction/event', 'App\Http\Controllers\NavigationController@event_index');