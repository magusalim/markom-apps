<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NavigationController extends Controller
{
    public function company_index()
    {
        return view('master.company.index');
    }

    public function unit_index()
    {
        return view('master.unit.index');
    }

    public function product_index()
    {
        return view('master.product.index');
    }

    public function event_index()
    {
        return view('transaction.event.index');
    }
}
