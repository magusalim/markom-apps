@extends('0_templates.apps')

@section('content')

<!-- Show Tables -->
<div class="card shadow mb-4 col-md-12">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-dark">Company Lists</h6>
    </div>
    <div class="card-body">
        <div class="table">
            <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                            <thead>
                                <tr role="row">
                                    <th>No</th>
                                    <th>Company Code</th>
                                    <th>Company Name</th>
                                    <th>Created Date</th>
                                    <th>Created By</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                {{-- @foreach ($data as $d)
                                <tr role="row" class="odd">
                                    <td>{{$d->id}}</td>
                                    <td>{{$d->code}}</td>
                                    <td>{{$d->name}}</td>
                                    <td>{{$d->created_at}}</td>
                                    <td>{{$d->created_by}}</td>
                                    <td>
                                        <a href="/edit_data/{{$d->id}}" class="btn btn-warning btn-block">
                                            <i class="fas fa-edit"></i> Edit
                                        </a>
                                    </td>
                                    <td>
                                        <form action="/delete_data/{{$d->id}}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger btn-block">
                                                <i class="fas fa-trash"></i> Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach --}}

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection